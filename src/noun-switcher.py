import argparse
import re
import random
import yaml
import spacy
import nltk.data
from nltk.tokenize import sent_tokenize
from pprint import pprint as pp


parser = argparse.ArgumentParser(description="Switch terms between two texts")
parser.add_argument("--t1", type=str, dest="text_1")
parser.add_argument("--t2", type=str, dest="text_2")

args = parser.parse_args()

text_1 = args.text_1
text_2 = args.text_2


WORDS_FILE_YAML = "k8s_list.yml"

with open(WORDS_FILE_YAML, 'r') as stream:
    try:
        word_list=yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

nlp = spacy.load("en_core_web_lg")

def map_lists(list_1, list_2):
    
    list_mapping = {}
   
    for index, item in enumerate(list_1):
        try:
            list_mapping[item] = list_2[index]
        except IndexError:
            list_mapping[item] = item
    
    return list_mapping

def map_random(list, random_list):
    
    list_mapping = {}
   
    for index, item in enumerate(list):
        if (index % 2) == 0:
            list_mapping[item] = random.choice(random_list)
        else:
            list_mapping[item] = item
    
    return list_mapping
       
def replace_words(text, word_mapping):
    """
    take a text and replace words that match a key in a dictionary with
    the associated value, return the changed text
    """
    rc = re.compile('|'.join(map(re.escape, word_mapping)))
    def translate(match):
        return word_mapping[match.group(0)]
    return rc.sub(translate, text)
                
        

def get_word_lists(text):
    doc = nlp(text)
    verbs = [token.lemma_ for token in doc if token.pos_ == "VERB"]
    nouns = [chunk.text for chunk in doc.noun_chunks]
    
    return verbs, nouns

verbs_1, nouns_1 = get_word_lists(text_1)
verbs_2, nouns_2 = get_word_lists(text_2)

nouns_mapping_1 = map_lists(nouns_1, nouns_2)
verbs_mapping_1 = map_lists(verbs_1, verbs_2)
random_explainer_mapping_1 = map_random(nouns_1, word_list)


nouns_mapping_2 = map_lists(nouns_2, nouns_1)
verbs_mapping_2 = map_lists(verbs_2, verbs_1)
random_explainer_mapping_2 = map_random(nouns_2, word_list)

text_1_swapped = replace_words(text_1, nouns_mapping_1)
text_2_swapped = replace_words(text_2, nouns_mapping_2)
text_1_random_explainer = replace_words(text_1, random_explainer_mapping_1)
text_2_random_explainer = replace_words(text_2, random_explainer_mapping_2)



output = {
        "swapped_text_1": text_1_swapped,
        "swapped_text_2": text_2_swapped,
        "random_explainer_1": text_1_random_explainer,
        "random_explainer_2": text_2_random_explainer
        }

pp(output)